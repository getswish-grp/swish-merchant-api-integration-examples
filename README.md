# Swish Merchant API Integration Examples
## cURL
### Create Payment Request
#### Request

```bash
curl -v --data '{ "payeePaymentReference": "0123456789", "callbackUrl": "https://example.com/api/swishcb/paymentrequests", "payerAlias": "4671234768", "payeeAlias": "1231181189", "amount": "100", "currency": "SEK", "message": "Kingston USB Flash Drive 8 GB" }' -H "Content-Type: application/json" POST https://mss.cpc.getswish.net/swish-cpcapi/api/v1/paymentrequests --cert "Swish Merchant Test Certificate 1231181189.p12:swish" --cert-type p12 --cacert "Swish TLS Root CA.pem"
```

#### Response

<pre>
< HTTP/1.1 201
< Location: https://mss.cpc.getswish.net/swish-cpcapi/api/v1/paymentrequests/5D59DA1B1632424E874DDB219AD54597
< Server: nginx/1.12.1
< Connection: keep-alive
< Content-Length: 0
< Date: Wed, 02 Jan 2019 14:29:51 GMT
<
* Connection #1 to host mss.cpc.getswish.net left intact
</pre>

### Get Payment Request
#### Request

```bash
curl -v "Content-Type: application/json" GET https://mss.cpc.getswish.net/swish-cpcapi/api/v1/paymentrequests/5D59DA1B1632424E874DDB219AD54597 --cert "Swish Merchant Test Certificate 1231181189.p12:swish" --cert-type p12 --cacert "Swish TLS Root CA.pem"
```

#### Response

<pre>
< HTTP/1.1 200
< Content-Type: application/json;charset=UTF-8
< Transfer-Encoding: chunked
< Date: Wed, 02 Jan 2019 14:33:25 GMT
<
* Connection #1 to host mss.cpc.getswish.net left intact
{"id":"5D59DA1B1632424E874DDB219AD54597","payeePaymentReference":"0123456789","paymentReference":"1E2FC19E5E5E4E18916609B7F8911C12","callbackUrl":"https://example.com/api/swishcb/paymentrequests","payerAlias":"4671234768","payeeAlias":"1231181189","amount":100.00,"currency":"SEK","message":"Kingston USB Flash Drive 8 GB","status":"PAID","dateCreated":"2019-01-02T14:29:51.092Z","datePaid":"2019-01-02T14:29:55.093Z","errorCode":null,"errorMessage":""}
</pre>

## PHP
### Create Payment Request

```php
<?php
	function createPaymentRequest() {
		$ch = curl_init('https://mss.cpc.getswish.net/swish-cpcapi/api/v1/paymentrequests');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, '1');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, '1');
		curl_setopt($ch, CURLOPT_CAINFO, 'Swish TLS Root CA.pem');
		curl_setopt($ch, CURLOPT_SSLCERT, 'Swish Merchant Test Certificate 1231181189.pem');
		curl_setopt($ch, CURLOPT_SSLKEY, 'Swish Merchant Test Certificate 1231181189.key');

		curl_setopt($ch, CURLOPT_HEADERFUNCTION,
	 		function($curl, $header) use (&$headers) {
				// this function is called by curl for each header received
		    	$len = strlen($header);
		    	$header = explode(':', $header, 2);
		    	if (count($header) < 2) {
		    		// ignore invalid headers
		      		return $len;
		    	} 

		    	$name = strtolower(trim($header[0]));
		    	echo "[". $name . "] => " . $header[1];

		    	return $len;
		 	 }
		);

		$data = array("payeePaymentReference" => "0123456789", "callbackUrl" => "https://example.com/api/swishcb/paymentrequests", "payerAlias" => "4671234768", "payeeAlias" => "1231181189", "amount" => "100", "currency" => "SEK", "message" => "Kingston USB Flash Drive 8 GB");          
		$data_string = json_encode($data);
                                                              
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);    
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
		    'Content-Type: application/json',                                                                                
		    'Content-Length: ' . strlen($data_string))                                                                       
		);                                                                                                                   

		if(!$response = curl_exec($ch)) { 
	        trigger_error(curl_error($ch)); 
	    }
		curl_close($ch);
	}

	createPaymentRequest();
?>
```

### Get Payment Request

```php
<?php
	function getPaymentRequest() {
		$ch = curl_init('https://mss.cpc.getswish.net/swish-cpcapi/api/v1/paymentrequests/5EAA11CBE0844071B0DBF81E9F4B0DB7');
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, '1');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, '1');
		curl_setopt($ch, CURLOPT_CAINFO, 'Swish TLS Root CA.pem');
		curl_setopt($ch, CURLOPT_SSLCERT, 'Swish Merchant Test Certificate 1231181189.pem');
		curl_setopt($ch, CURLOPT_SSLKEY, 'Swish Merchant Test Certificate 1231181189.key');

		curl_setopt($ch, CURLOPT_HEADERFUNCTION,
	 		function($curl, $header) use (&$headers) {
				// this function is called by curl for each header received
		    	$len = strlen($header);
		    	$header = explode(':', $header, 2);
		    	if (count($header) < 2) {
		    		// ignore invalid headers
		      		return $len;
		    	} 

		    	$name = strtolower(trim($header[0]));
		    	echo "[". $name . "] => " . $header[1];

		    	return $len;
		 	 }
		);                                                                                                               

		if(!$response = curl_exec($ch)) { 
	        trigger_error(curl_error($ch)); 
	    }
		curl_close($ch);
	}

	getPaymentRequest();
?>
```

## Java
### Create Payment Request

```java
public static String createPaymentRequest() {
	System.setProperty("javax.net.ssl.keyStore", "./Swish Merchant Test Certificate 1231181189.p12");
	System.setProperty("javax.net.ssl.keyStorePassword", "swish");
	System.setProperty("javax.net.ssl.keyStoreType", "PKCS12");

	System.setProperty("javax.net.ssl.trustStore", "./chain.jks");
	System.setProperty("javax.net.ssl.trustStorePassword", "swish123");
	System.setProperty("javax.net.ssl.trustStoreType", "JKS");

	HttpsURLConnection connection = null;

  	try {
	    URL url = new URL("https://mss.cpc.getswish.net/swish-cpcapi/api/v1/paymentrequests");

	    connection = (HttpsURLConnection) url.openConnection();
	    SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
	    connection.setSSLSocketFactory(sslsocketfactory);
	    connection.setRequestMethod("POST");

		String json = "{ \"payeePaymentReference\": \"0123456789\", \"callbackUrl\": \"https://example.com/api/swishcb/paymentrequests\", \"payerAlias\": \"4671234768\", \"payeeAlias\": \"1231181189\", \"amount\": \"100\", \"currency\": \"SEK\", \"message\": \"Kingston USB Flash Drive 8 GB\" }";
	    connection.setRequestProperty("Content-Type", "application/json");
	    connection.setRequestProperty("Content-Length", Integer.toString(json.length()));

	    connection.setUseCaches(false);
	    connection.setDoOutput(true);

	    DataOutputStream wr = new DataOutputStream (connection.getOutputStream());
	    wr.writeBytes(json);
	    wr.close();

	    InputStream is = connection.getInputStream();
	    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
	    StringBuilder response = new StringBuilder();
	    String line;
	    while ((line = rd.readLine()) != null) {
      		response.append(line);
      		response.append('\r');
	    }
	    rd.close();

    	// Print response headers
     	Map<String, List<String>> headers = connection.getHeaderFields();
        Set<Map.Entry<String, List<String>>> entrySet = headers.entrySet();
        for (Map.Entry<String, List<String>> entry : entrySet) {
            String headerName = entry.getKey();
            List<String> headerValues = entry.getValue();
            System.out.println("[" + headerName + "] => " + headerValues.get(0));
        }
	    System.out.println(response);

	    return response.toString();
  	} catch (Exception e) {
	    e.printStackTrace();
	    return null;
  	} finally {
    	if (connection != null) {
      		connection.disconnect();
	    }
  	}
}
```

### Get Payment Request

```java
public static String getPaymentRequest() {
	System.setProperty("javax.net.ssl.keyStore", "./Swish Merchant Test Certificate 1231181189.p12");
	System.setProperty("javax.net.ssl.keyStorePassword", "swish");
	System.setProperty("javax.net.ssl.keyStoreType", "PKCS12");

	System.setProperty("javax.net.ssl.trustStore", "./chain.jks");
	System.setProperty("javax.net.ssl.trustStorePassword", "swish123");
	System.setProperty("javax.net.ssl.trustStoreType", "JKS");

	HttpsURLConnection connection = null;

  	try {
	    URL url = new URL("https://mss.cpc.getswish.net/swish-cpcapi/api/v1/paymentrequests/5EAA11CBE0844071B0DBF81E9F4B0DB7");

	    connection = (HttpsURLConnection) url.openConnection();
	    SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
	    connection.setSSLSocketFactory(sslsocketfactory);
	    connection.setRequestMethod("GET");

	    connection.setUseCaches(false);
	    connection.setDoOutput(true);

	    InputStream is = connection.getInputStream();
	    BufferedReader rd = new BufferedReader(new InputStreamReader(is));
	    StringBuilder response = new StringBuilder();
	    String line;
	    while ((line = rd.readLine()) != null) {
      		response.append(line);
      		response.append('\r');
	    }
	    rd.close();
	    System.out.println(response);

	    return response.toString();
  	} catch (Exception e) {
	    e.printStackTrace();
	    return null;
  	} finally {
    	if (connection != null) {
      		connection.disconnect();
	    }
  	}
}
```
